package org.nrg.ccf.asp.services;

/*
    Queries a remote XNAT to see what image sessions are there for a project,
    finds the difference between that and local project, and then does an
    upload for each image session not present on destination.
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.nrg.ccf.asp.prefs.AsperaSitePrefs;
import org.nrg.ccf.asp.utils.AsperaClient;
import org.nrg.config.entities.Configuration;
import org.nrg.config.services.ConfigService;
import org.nrg.framework.constants.Scope;
//import org.nrg.xsync.remote.alias.services.RemoteAliasService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class AsperaSendService {

    @Autowired
    public AsperaSendService(/*final RemoteAliasService remoteAliasService,*/
                             final ConfigService configService,
                             final AsperaSitePrefs prefs) {
//        _localProjectId = localProjectId;
//        _remoteAliasService = remoteAliasService;
        _configService = configService;
        _prefs = prefs;
    }

    private JsonNode getConfigObject() {
        /////////////////////////
        String projectId = "NKI";
        /////////////////////////
        final Configuration conf = _configService.getConfig("aspera", "json", Scope.Project, projectId);
        String config = conf != null ? conf.getContents() : null;
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode node = objectMapper.createObjectNode();
//        JsonNode node = null;

        if (StringUtils.isNotBlank(config)) {
            try {
                // TODO Make less deplorable
//                config = config.replace("\\n", "");
//                config = config.replaceAll("\\\\", "");
//                config = config.replaceAll("^\"|\"$", "");
                node = objectMapper.readTree(config);
            } catch (Exception e) {}
        }
        return node;
    }

    private List<String> getImageSessionDiff(String projectId) {
        List<String> sessions = null;

        // TESTING
        sessions.add("0102157");
        sessions.add("0103645");
        // TESTING

        return sessions;
    }

    public void sendImageSession(String projectId, String sessionLabel) throws IOException, InterruptedException {
        // Create new client instance for every image session
        AsperaClient _asperaClient = new AsperaClient(_prefs);
        _logger.info("Uploading " + sessionLabel + " for project " + projectId);

        if (_asperaClient.isRunning(projectId, sessionLabel)) {
            _asperaClient.status.setStatus("conflict");
            _logger.error("Already sending session " + sessionLabel + " for project " + projectId);
            return;
        }

        // TODO shouldn't be based on upload success since we don't want to wait for its return value
        _asperaClient.upload(projectId, sessionLabel);

//        if (success) {
//            _logger.info(sessionLabel + " successfully sent");
//            _asperaClient.status.setStatus("success");
//        } else {
//            _logger.error("Aspera send failed for " + sessionLabel);
//            _asperaClient.status.setStatus("error");
//        }
    }

    public void sendImageSessions(String projectId) throws IOException, InterruptedException {
        List<String> missingSessions = getImageSessionDiff(projectId);
//        AsperaClient _asperaClient = new AsperaClient(_prefs);

        for (String missingSession : missingSessions) {
            this.sendImageSession(projectId, missingSession);
        }
    }

    private static final Logger _logger = LoggerFactory.getLogger(AsperaSendService.class);
    private final ConfigService _configService;
//    private final RemoteAliasService _remoteAliasService;
    private final JsonNode _config = getConfigObject();
    private final AsperaSitePrefs _prefs;
}
