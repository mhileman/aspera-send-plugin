package org.nrg.ccf.asp.xapi;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.lang3.StringUtils;
import org.nrg.ccf.asp.prefs.AsperaSitePrefs;
import org.nrg.ccf.asp.services.AsperaSendService;
import org.nrg.ccf.asp.utils.AsperaStatus;
import org.nrg.config.entities.Configuration;
import org.nrg.config.services.ConfigService;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.framework.constants.Scope;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xsync.remote.alias.services.RemoteAliasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import static org.nrg.xdat.security.helpers.AccessLevel.Admin;

@XapiRestController
@RequestMapping(value = "/aspera")
@Api(description = "Aspera XNAT API")
public class AsperaSendApi extends AbstractXapiRestController {

    @Autowired
    protected AsperaSendApi(UserManagementServiceI userManagementService,
                            RoleHolder roleHolder,
                            final AsperaSitePrefs prefs,
                            final ConfigService configService) {
//                            final RemoteAliasService remoteAliasService) {
        super(userManagementService, roleHolder);
        _prefs = prefs;
        _configService = configService;
//        _remoteAliasService = remoteAliasService;
    }

    @XapiRequestMapping(value = "/config", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Gets Aspera Node configuration", response = Properties.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Aspera config retrieved"),
            @ApiResponse(code = 500, message = "Unexpected error retrieving Aspera config")})
    public ResponseEntity<Properties> getPreferences() throws NrgServiceException {
        try {
            final Properties preferences = new Properties();
            preferences.setProperty("asperaNodeUrl", _prefs.getAsperaNodeUrl());
            preferences.setProperty("asperaNodeUser", _prefs.getAsperaNodeUser());
            preferences.setProperty("privateKey", _prefs.getPrivateKey());
            preferences.setProperty("destinationDirectory", _prefs.getDestinationDirectory());
            preferences.setProperty("logDirectory", _prefs.getLogDirectory());
            preferences.setProperty("sshPort", _prefs.getSshPort());
            preferences.setProperty("udpPort", _prefs.getUdpPort());

            // TODO try returning _prefs instead of making new preferences object
            return new ResponseEntity<>(preferences, HttpStatus.OK);
        } catch (Exception e) {
            throw new NrgServiceException("Aspera config retrieval failed", e);
        }
    }

    @XapiRequestMapping(value = "/config", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, restrictTo = Admin)
    @ApiOperation(value = "Sets the Aspera Node configuration")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Aspera Node site config set"),
            @ApiResponse(code = 500, message = "Unexpected error")})
    public ResponseEntity<String> setPreferences(@RequestBody String jsonbody) throws IOException {
        final ObjectMapper objectMapper = new ObjectMapper();
        final JsonNode asperaConfigJson = objectMapper.readValue(jsonbody, JsonNode.class);

        try {
            final String asperaNodeUrl = asperaConfigJson.get("asperaNodeUrl").asText();
            final String asperaNodeUser = asperaConfigJson.get("asperaNodeUser").asText();
            final String privateKey = asperaConfigJson.get("privateKey").asText();
            final String destinationDirectory = asperaConfigJson.get("destinationDirectory").asText();
            final String logDirectory = asperaConfigJson.get("logDirectory").asText();
            final String sshPort = asperaConfigJson.get("sshPort").asText();
            final String udpPort = asperaConfigJson.get("udpPort").asText();
            _prefs.setAsperaNodeUrl(asperaNodeUrl);
            _prefs.setAsperaNodeUser(asperaNodeUser);
            _prefs.setPrivateKey(privateKey);
            _prefs.setDestinationDirectory(destinationDirectory);
            _prefs.setLogDirectory(logDirectory);
            _prefs.setSshPort(sshPort);
            _prefs.setUdpPort(udpPort);
        } catch (Exception e) {
            return new ResponseEntity<>("Aspera config setup failed", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>("Aspera config saved", HttpStatus.OK);
    }

    @ApiOperation(value = "Gets Aspera Node configuration")
    @XapiRequestMapping(value = "/project/{projectId}/config", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiResponses({
            @ApiResponse(code = 200, message = "Aspera config retrieved"),
            @ApiResponse(code = 500, message = "Unexpected error retrieving Aspera config")})
    public ResponseEntity<JsonNode> getProjectPreferences(@PathVariable String projectId) throws NrgServiceException {
        final Configuration conf = _configService.getConfig("aspera", "json", Scope.Project, projectId);
        String config = conf != null ? conf.getContents() : null;
        ObjectMapper objectMapper = new ObjectMapper();

        if (StringUtils.isNotBlank(config)) {
            try {
                // TODO Make less deplorable
                config = config.replace("\\n", "");
                config = config.replaceAll("\\\\", "");
                config = config.replaceAll("^\"|\"$", "");
                JsonNode node = objectMapper.readTree(config);
                return new ResponseEntity<>(node, HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @XapiRequestMapping(value = "/project/{projectId}/config", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, restrictTo = Admin)
    @ApiOperation(value = "Sets the Aspera Node configuration")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Aspera project config set"),
            @ApiResponse(code = 500, message = "Unexpected error")})
    public ResponseEntity<String> setProjectPreferences(@PathVariable final String projectId, @RequestBody String jsonbody) throws IOException {
        try {
            final Gson _gson = new Gson();
            final String configJson = _gson.toJson(jsonbody);

            _configService.replaceConfig(this.getSessionUser().getUsername(), "",  "aspera", "json", configJson, Scope.Project, projectId);
        } catch (Exception e) {
            return new ResponseEntity<>("Aspera config setup failed", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>("Aspera config saved", HttpStatus.OK);
    }

    // TODO test asynchronous
    // @Async
    @XapiRequestMapping(value = "/project/{projectId}/experiment/{sessionLabel}/send", method = RequestMethod.PUT)
    @ApiOperation(value = "Send session to configured Aspera Node", response = String.class)
    @ResponseBody
    public ResponseEntity<String> sendSession(@PathVariable final String sessionLabel, @PathVariable final String projectId)
            throws IOException, InterruptedException {

        AsperaSendService sendService = new AsperaSendService(_configService, _prefs);
//        AsperaSendService sendService = new AsperaSendService(_remoteAliasService, _configService, _prefs);

        sendService.sendImageSession(projectId, sessionLabel);

//        // Create new client instance for every connection
//        AsperaClient _asperaClient = new AsperaClient(_prefs);
//        _logger.info("Uploading " + sessionLabel + " for project " + projectId);
//
//        String responseBody = "";
//        if (_asperaClient.isRunning(projectId, sessionLabel)) {
//            _asperaClient.status.setStatus("conflict");
//            responseBody = "Already sending session " + sessionLabel + " for project " + projectId;
//            _logger.error(responseBody);
//            return new ResponseEntity<>(responseBody, HttpStatus.CONFLICT);
//        }
//
//        // TODO shouldn't be based on upload success since we don't want to wait for its return value
//        boolean success = _asperaClient.upload(projectId, sessionLabel);
//
//        if (success) {
//            return new ResponseEntity<>(responseBody, HttpStatus.OK);
//        } else {
//            responseBody = "Aspera send unsuccessful";
//            return new ResponseEntity<>(responseBody, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @XapiRequestMapping(value = "/project/{projectId}/send", method = RequestMethod.PUT)
    @ApiOperation(value = "Send session to configured Aspera Node", response = String.class)
    @ResponseBody
    public ResponseEntity<Void> sendProject( @PathVariable final String projectId)
            throws IOException, InterruptedException {

//        AsperaSendService sendService = new AsperaSendService(_remoteAliasService, _configService, _prefs);
        AsperaSendService sendService = new AsperaSendService(_configService, _prefs);
        sendService.sendImageSessions(projectId);

//        _logger.info("Uploading " + sessionLabel + " for project " + projectId);
//        String sessionDirectory = String.format("/data/intradb/archive/%s/arc001/%s", projectId, sessionLabel);
//
//        _asperaClient.upload(sessionDirectory);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @XapiRequestMapping(value = "/status", method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Monitor Aspera transfer for a site", response = ArrayList.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Aspera status retrieved"),
            @ApiResponse(code = 500, message = "Unexpected error retrieving Aspera status")})
    public ResponseEntity<ArrayList<AsperaStatus>> siteStatus() {
        ArrayList<AsperaStatus> siteStatuses = new ArrayList<>();

        for (int i = 0; i < AsperaStatus.list.size(); i++) {
            AsperaStatus s = AsperaStatus.list.get(i);

            if (s.getProject() != null) {
                siteStatuses.add(s);
            }
        }
        return new ResponseEntity<>(siteStatuses, HttpStatus.OK);
    }

    @XapiRequestMapping(value = "/project/{projectId}/status", method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Monitor Aspera transfer for a project", response = ArrayList.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Aspera status retrieved"),
            @ApiResponse(code = 500, message = "Unexpected error retrieving Aspera status")})
    public ResponseEntity<ArrayList<AsperaStatus>> projectStatus(@PathVariable final String projectId) {
        ArrayList<AsperaStatus> projectStatuses = new ArrayList<>();

        for (int i = 0; i < AsperaStatus.list.size(); i++) {
            AsperaStatus s = AsperaStatus.list.get(i);

            if (s.getProject() != null && projectId.equals(s.getProject())) {
                projectStatuses.add(s);
            }
        }
        return new ResponseEntity<>(projectStatuses, HttpStatus.OK);
    }

    @XapiRequestMapping(value = "/project/{projectId}/experiment/{sessionLabel}/status", method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Monitor Aspera transfer for a session", response = AsperaStatus.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Aspera status retrieved"),
            @ApiResponse(code = 500, message = "Unexpected error retrieving Aspera status")})
    public ResponseEntity<AsperaStatus> sessionStatus(
            @PathVariable final String projectId, @PathVariable final String sessionLabel) {

        for (int i = 0; i < AsperaStatus.list.size(); i++) {
            AsperaStatus s = AsperaStatus.list.get(i);

            if (s.getProject() != null && projectId.equals(s.getProject()) && sessionLabel.equals(s.getSession())) {
                return new ResponseEntity<>(s, HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(new AsperaStatus(), HttpStatus.OK);
    }

//    private static final Logger _logger = LoggerFactory.getLogger(AsperaSendApi.class);
    private final ConfigService _configService;
    private final AsperaSitePrefs _prefs;
//    private final RemoteAliasService _remoteAliasService;
}
