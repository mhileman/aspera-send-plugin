package org.nrg.ccf.asp.conf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
        value = "asperaSendPlugin",
        name = "Aspera Send Plugin"
//        entityPackages = "org.nrg.ccf.pcp.entities"
)
@ComponentScan({
        "org.nrg.ccf.asp.conf",
        "org.nrg.ccf.asp.xapi",
        "org.nrg.ccf.asp.utils",
        "org.nrg.ccf.asp.prefs",
        "org.nrg.ccf.asp.services"
})

public class AsperaSendPlugin {
    private static Logger logger = LoggerFactory.getLogger(AsperaSendPlugin.class);

    public AsperaSendPlugin() {
        logger.info("Configuring the Aspera Send plugin.");
    }
}
