if (typeof CCF === 'undefined') {
    CCF = {};
}
if (typeof CCF.aspera === 'undefined') {
    CCF.aspera = {
        project: XNAT.data.projectId,
        remoteUrl: 'http://'
    };
}


(function(){
    /*
    Remote Authentication
    */

    /*
     * Get user credentials
     * @param {String} optional configuration JSON to submit if initial setup
     */

    function enterCredentials(configJson){

        var remoteProjectId = $("#remote-project").val() ||
            CCF.aspera.project ||
            "ERROR";

        var remoteHost =
            CCF.aspera.remoteUrl !== "http://" ?
                CCF.aspera.remoteUrl :
                $("#remote-xnat-host").val();

        var modalContent = '' +
            '<form id="aspera-remote-credentials">' +
                '<h3 class="credentials-header">Enter credentials for: <span class="remote-site">' + remoteHost + '</span></h3>' +
                '<input type="hidden" name="host" value="' + remoteHost + '">' +
                '<input type="hidden" name="method" value="GET">' +
                '<label class="credentials-input">' +
                '<b>Username: </b>' +
                '<input id="aspera-remote-user" type="text" name="username" size="24">' +
                '</label><br>' +
                '<label class="credentials-input">' +
                '<b>Password: </b>' +
                '<input id="aspera-remote-pass" type="password" name="password" size="24">' +
                '</label>' +
            '</form>' +
            '';

        var modalOpts = {
            width: 480,
            height: 360,
            id: 'xmodal-enter-credentials',
            title: "Enter remote server credentials",
            content: modalContent,
            afterShow: function(){
                this.$modal.find('#xsync-credentials-username').focus();
            },
            ok: 'show',
            okLabel: 'Continue',
            okAction: function(modl){

                var $form = modl.$modal.find('#aspera-remote-credentials');

                // var remoteHost = $form.find('[name="host"]').val();
                // var remoteUser = $form.find('[name="username"]').val();
                // var remotePassword = $form.find('[name="password"]').val();
                var remoteUser = $("#aspera-remote-user").val();
                var remotePassword = $("#aspera-remote-pass").val();

                var tokenData = {
                    url: remoteHost + '/data/services/tokens/issue',
                    method: 'GET',
                    username: remoteUser,
                    password: remotePassword
                };

                var credentialsAjax = $.ajax({
                    type: 'POST',
                    url: XNAT.url.csrfUrl('/xapi/xsync/remoteREST'),
                    dataType: 'json',
                    data: JSON.stringify(tokenData),
                    processData: false,
                    contentType: 'application/json'
                });

                credentialsAjax.done(function(data){

                    if (typeof data !== 'undefined' && typeof data.secret !== 'undefined') {

                        var formData = {
                            host: remoteHost,
                            localProject: XNAT.data.projectId,
                            remoteProject: remoteProjectId,
                            alias: data.alias,
                            secret: data.secret,
                            username: remoteUser
                        };
                        try {
                            formData['estimatedExpirationTime'] = data.estimatedExpirationTime
                        }
                        catch (err) {}

                        var saveCredentials = $.ajax({
                            type: 'POST',
                            url: XNAT.url.csrfUrl('/xapi/xsync/credentials/save/projects/' + XNAT.data.projectId),
                            dataType: 'text',
                            data: JSON.stringify(formData),
                            processData: false,
                            contentType: 'text/plain'
                        });

                        saveCredentials.done(function(data, textStatus, jqXHR) {
                            if (jqXHR.status === 200) {
                                xmodal.message(
                                    'Credentials saved for ' + remoteProjectId + ' on ' + remoteHost
                                )
                            }
                            else if (jqXHR.status === 202) {
                                xmodal.message(
                                    'Credentials saved', ' WARNING: ' + jqXHR.responseText + '\n' +
                                    'Successfully saved credentials for remote server ' + remoteHost
                                );
                            }
                            else {
                                xmodal.message(
                                    'Credentials not saved', jqXHR.status + ': ' + jqXHR.responseText
                                )
                                // if (XSYNC.xsyncconfig.firsttime === false) {
                                //     xmodal.message(
                                //         'Credentials saved', 'Successfully saved credentials for remote server ' +
                                //         remoteHost
                                //     );
                                // }
                            }
                            modl.close();

                            // submit the config json again if this was called from submitConfig
                            // if (configJson !== undefined) {
                            //     XSYNC.xsyncconfig.firsttime = false;
                            //     XSYNC.xsyncconfig.submitConfig(configJson)
                            // }
                        });

                        saveCredentials.fail(function(data, textStatus, jqXHR){
                            xmodal.message(
                                'Error', 'Could not save credentials for remote server ' +
                                remoteHost + ' Cause: ' + data.statusText + " Details: " + data.responseText
                            );
                            modl.close();
                        });

                    }
                    else {
                        console.log(XNAT.url.csrfUrl('/xapi/xsync/credentials/save/projects/' + XNAT.data.projectId));
                        xmodal.message('Error', 'ERROR:  Could not get alias token.  Please check username and password and try again.');
                    }

                });
                credentialsAjax.fail(function(data, textStatus, error){
                    console.log(XNAT.url.csrfUrl('/xapi/xsync/credentials/save/projects/' + XNAT.data.projectId));
                    xmodal.message('Error', 'ERROR:  Could not get alias token.  Please check username and password and try again.');
                });

            },
            okClose: false,
            cancel: 'Cancel',
            cancelLabel: 'Cancel',
            cancelAction: function(){
                xmodal.close(XNAT.app.abu.abuConfigs.modalOpts.id);
            },
            closeBtn: 'hide'
        };

        xmodal.open(modalOpts);

    }

    CCF.aspera.enterCredentials = enterCredentials;


    function checkCredentials(callback, failure){

        this.checkCredentialsResult = false;

        var formData = {
            host: $('#aspera-remote-url').val(),
            localProject: XNAT.data.projectId
        };

        var saveCredentials = $.ajax({
            type: "POST",
            url: XNAT.url.csrfUrl('/xapi/xsync/credentials/check/projects/' + XNAT.data.projectId),
            cache: false,
            async: false,
            dataType: 'text',
            data: JSON.stringify(formData),
            processData: false,
            contentType: "text/plain"
        });

        saveCredentials.done(function(data, textStatus, jqXHR){
            // XSYNC.xsyncconfig.checkCredentialsResult = true;
            if (typeof callback === 'function') {
                callback();
            }
        });

        saveCredentials.fail(function(data, textStatus){
            console.log(textStatus + " - Failed to save credentials");
            if (typeof failure === 'function') {
                failure();
            }
        });

        return saveCredentials;
    }
    CCF.aspera.checkCredentials = checkCredentials;

})();
